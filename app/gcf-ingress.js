  const {Storage} = require('@google-cloud/storage');
  const PubSub = require('@google-cloud/pubsub');
  const xmlParser = require('fast-xml-parser'); // pretty fast xml parser with minimum exernal lib deps

  // Crete pub/sub client instance
  const pubSubClient = new PubSub();

  // Crete GCS client instance
  const storage = new Storage();

  /**
   * Triggered from a message on a Cloud Pub/Sub topic.
   *
   * @param {!Object} event Event payload.
   * @param {!Object} context Metadata for the event.
   */
  exports.helloPubSub = (event, context) => {

    const dstTopicName = 'data-xy-egress'; // ##IMPORTANT## Change as required whatever your topic name is

    const bucketId = event.attributes.bucketId;
    const objectId = event.attributes.objectId;

    const bucket = storage.bucket(bucketId);
    const file = bucket.file(objectId);

    // Download file into the memory
    file.download(function(err, content) {
      /* Handle error
      ..
      .. Sample block below
      */
      if (err) {
        console.error(err);
        throw new Error(err); // This will cause cold start, so you probably want to catch the exception
      }

      let jsonData = xmlParser.parse(content.toString(), {}, true);
      // Assume you want to manipulate with the content before the next hop
      jsonData.others = 'extra field value'; // added new field "others"
      delete jsonData.note.heading; // removed a field called "heading"

      console.log(JSON.stringify(jsonData)); // for debug purpose, may remove in prod

      function publishMessageWithCustomAttributes() {
        // Publishes the message as a string
        const dataBuffer = Buffer.from(JSON.stringify(jsonData));

        // [Optional] Add two custom attributes to the message
        const customAttributes = {
          origin: 'nodejs-sample',
          operation: 'xml-etl',
        };

        return pubSubClient
          .topic(dstTopicName)
          .publisher()
          .publish(dataBuffer, customAttributes)
          .then(result => {
            const messageId = result;
            console.log(`Message ${messageId} published.`);

            return messageId;
          })
          .catch(err => {
            console.err('ERROR:', err);
          });
        
      }

      publishMessageWithCustomAttributes();

    });
      
  };
