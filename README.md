# gcp-pubsub-gcf-nodejs-sample

##### This code is not intended for production use. It's published for learning and research purpose only. No warranty provided. Please read the disclaimer below.

Repository contains nodejs code examples that can be used in Google Cloud Functions with NodeJS 10 runtime environment.

Sample nodejs code demonstrates:
1. How Google Cloud Function can receive and process Google Cloud Pub/Sub event triggered by new object in Cloud Storage bucket (GCS).
2. How Google Cloud Function can perform simple manipulation (e.g. transform xml to json, add/remove properties) and further publish the data to the next Pub/Sub topic that will deliver final message/data to its subscribers.

## Content & structure
There are two folders in this repository.

`./app` folder contains sample code and list of dependencies that are fully compatible with minimal required version of `@google-cloud/pubsub` JS library that is used by Cloud Functions *^0.18.0*

`./app-latest` folder contains sample code and list of dependencies that leverage two versions of `@google-cloud/pubsub` library:
* The default *^0.18.0*, required by Cloud Functions and used to process pub/sub trigger event.
* The latest stable *^2.5.0*, imported manually and used to publish processed data to the next pub/sub topic. This version of library enables the use of elegant and more modern async programming style. Import of two versions of the same library is achieved by using package aliasing inside `package.json`

I haven't tested if Cloud Function will work solely with `@google-cloud/pubsub` *v2.5.0* or not.

**Content**
* `gcf-ingress.js` - nodejs code for primary Cloud Function that is triggered by pub/sub topic when new object is created in Cloud Storage
* `gcf-egress.js` - nodejs code for the second Cloud Function that is triggered when ingress function processes data and publishes result to the next pub/sub topic.
* `package.json` - dependencies.
* `note.xml` - sample xml file that will be uploaded to GCS bucket where pub/sub notifications are enabled.

## Assumptions & prerequisites:

* IAM: appropriate service accounts with required roles have been setup. Make sure you Cloud Functions can download objects from Cloud Storage, and publish messages to Pub/Sub topics.
* Google Cloud Storage: bucket was setup with correct ACLs/permissions.
* Google Cloud Pub/Sub topics have been created and triggers for Cloud Functions configured accordingly.

Publish GCS notification to Cloud Pub/Sub when new objects are created in the bucket:
```
gsutil notification create -t [TOPIC_NAME] -f json -e OBJECT_FINALIZE gs://[BUCKET_NAME]
```

Publish GCS notifications to Cloud Pub/Sub for all object evensts in the bucket:
```
gsutil notification create -t [TOPIC_NAME] -f json gs://[BUCKET_NAME]
```

### Sample scenario
In my case, sample configuration looked like so:

* Single GCS bucket where I upload sample xml file `note.xml` (with bucket notifications enabled).
* First Pub/Sub topic called `data-xy-ingest` where GCS will publish events related to the bucket.
* First Cloud Function with name `data-xy-ingest-process` that is triggered by new event in topic `data-xy-ingest`.
* Second Pub/Sub topic called `data-xy-egress` where Cloud Function `data-xy-ingest-process` will publish final data.
* Second Cloud Function with name `data-xy-egress-process` that is triggered by the event in topic `data-xy-egress` containing processed data (xml transformed into JSON with some minor modifications).

## Disclaimer

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
